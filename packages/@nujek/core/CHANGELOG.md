# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.8](https://gitlab.com/nujek/nujek-monorepo/compare/@nujek/core@0.0.7...@nujek/core@0.0.8) (2019-10-22)

**Note:** Version bump only for package @nujek/core





## [0.0.7](https://gitlab.com/nujek/nujek-monorepo/compare/@nujek/core@0.0.6...@nujek/core@0.0.7) (2019-10-22)

**Note:** Version bump only for package @nujek/core





## [0.0.6](https://gitlab.com/nujek/nujek-monorepo/compare/@nujek/core@0.0.5...@nujek/core@0.0.6) (2019-10-22)

**Note:** Version bump only for package @nujek/core





## [0.0.5](https://gitlab.com/nujek/nujek-monorepo/compare/@nujek/core@0.0.4...@nujek/core@0.0.5) (2019-10-22)

**Note:** Version bump only for package @nujek/core





## [0.0.4](https://gitlab.com/nujek/nujek-monorepo/compare/@nujek/core@0.0.3...@nujek/core@0.0.4) (2019-10-22)

**Note:** Version bump only for package @nujek/core





## 0.0.3 (2019-10-22)

**Note:** Version bump only for package @nujek/core





## 0.0.2 (2019-10-22)

**Note:** Version bump only for package @nujek/core
