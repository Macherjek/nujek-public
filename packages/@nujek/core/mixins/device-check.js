/* This mixins has an depdency to @nuxtjs/device-module */

export default {
    mounted() {
      if (this.$device.isMobileOrTablet) {
        let shadesEl = document.querySelectorAll("html");
        if (shadesEl.length > 0) {
          shadesEl[0].classList.add("isMobile");
        }
      } else {
        let shadesEl = document.querySelectorAll("html");
        if (shadesEl.length > 0) {
          shadesEl[0].classList.add("isDesktop");
        }
      }
    }
  };
  