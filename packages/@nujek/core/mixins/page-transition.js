import TimelineMax from "gsap/TimelineMax";

export default {
  transition: {
    mode: "out-in",
    css: false,
    enter(el, done) {
      let tl = new TimelineMax({ onComplete: done });
      tl.to(".site", 0.5, { autoAlpha: 0 });
    },
    leave(el, done) {
      let tl = new TimelineMax({ onComplete: done });
      // tl.to('img', 0, {position: "absolute"}, 0)
      tl.to(".site", 0.5, { autoAlpha: 1 });
    }
  }
};
