// import { mapActions, mapState } from "vuex";
import _ from "lodash";
const OFFSET = 350;
import locomotiveScroll from "locomotive-scroll";
import "locomotive-scroll/dist/locomotive-scroll.min.css";


export default {
  name: "smooth-scroll",
  data() {
    return {
      scrollPosition: 0,
      top: 0
    };
  },
  mounted() {
    console.info("switchLogo: " + this.$refs.switchlogo);
    console.info("pageLogoColor: " + this.pageLogoColor);
    if (this.$refs.switchlogo != null) {
      this.top = this.$refs.switchlogo.getBoundingClientRect().top;
      //this.setLogoColor("light");
    }
    this.$nextTick(
      function() {
        this.lmS = new locomotiveScroll({
          el: document.querySelector("#js-scroll"),
          smooth: true /* if false disable overflow: hidden on html, body */
        });
        this.lmS.on("scroll", _.throttle(this.onLmsScroll, 150));
        window.addEventListener(
          "resize",
          _.debounce(
            function() {
              this.lmS.update();
            }.bind(this),
            100
          )
        );
        //this.growNav();
      }.bind(this)
    );
  },
  destroyed() {
    if(typeof this.lmS !== "undefined") {
      this.lmS.destroy();
      window.removeEventListener("resize", this.onLmsResize);
    }
  },
  computed: {
    // ...mapState({
    //   navIsShrinked: state => state.nav.navIsShrinked,
    //   pageLogoColor: state => state.nav.pageLogoColor
    // })
  },
  methods: {
    onLmsScroll(obj) {
      // this.scrollPosition = obj["scroll"]["y"];
      // if (!this.$device.isMobile) {
      //   if (this.scrollPosition < OFFSET) {
      //     if (this.navIsShrinked) {
      //       this.growNav();
      //     }
      //   } else {
      //     if (!this.navIsShrinked) {
      //       this.shrinkNav();
      //     }
      //   }
      // }

      // console.info("sp: " + this.scrollPosition + "top: " + this.top);
      // if (this.scrollPosition > this.top) {
      //   //console.info("should be dark");
      //   if (this.pageLogoColor !== "dark") {
      //     console.info("Set dark");
      //     this.setLogoColor("dark");
      //   }
      // } else {
      //   //console.info("should be light");
      //   if (this.pageLogoColor !== "light") {
      //     console.info("Set light");
      //     this.setLogoColor("light");
      //   }
      // }
    },
    // ...mapActions({
    //   shrinkNav: "nav/shrinkNav",
    //   growNav: "nav/growNav",
    //   setLogoColor: "nav/setLogoColor"
    // })
  }
};
