const path = require("path");

export default {
  head: {
    script: [
      {
        src:
          "https://polyfill.io/v3/polyfill.min.js?features=NodeList.prototype.forEach%2CArray.prototype.forEach%2CElement.prototype.append%2CIntersectionObserver%2CElement.prototype.prepend%2CCustomEvent%2CEvent%2CElement.prototype.remove"
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: false,
  /*
   ** Global CSS
   */
  css: [
    path.resolve(__dirname, "./styles/index.css")
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@bazzite/nuxt-optimized-images',
    '@nuxtjs/device'
  ],

  optimizedImages: {
    inlineImageLimit: -1,
    handleImages: ["jpeg", "png", "svg", "webp", "gif"],
    optimizeImages: true,
    optimizeImagesInDev: false,
    defaultImageLoader: "img-loader",
    mozjpeg: {
      quality: 85
    },
    optipng: false,
    pngquant: {
      speed: 7,
      quality: [0.65, 0.8]
    },
    webp: {
      quality: 85
    }
  },

  /*
   ** Build configuration
   */
  build: {
    transpile: ["TweenMax", "TimelineMax"],

    // transpile: ['vue-instantsearch', 'instantsearch.js/de'],
    postcss: {
      // extractCSS: true,
      plugins: {
        "postcss-import": {},
        // @TODO: Check if this works with example outside monorepo
        tailwindcss: path.resolve("./tailwind.config.js"),
        rfs: {
          twoDimensional: false,
          baseValue: 16,
          unit: "rem",
          breakpoint: 1920,
          breakpointUnit: "px",
          factor: 100,
          class: false,
          unitPrecision: 6,
          safariIframeResizeBugFix: false,
          remValue: 16
        },
        "postcss-advanced-variables": {},
        "postcss-nested": {},
        "postcss-hexrgba": {},
        "postcss-percentage": {}
      },
      // Change the postcss-preset-env settings
      preset: {
        stage: 0,
        autoprefixer: {
          cascade: false,
          grid: true
        }
      }
    },
    /*
     ** You can extend webpack config here
     */

    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });

        ctx.loaders.vue.transformAssetUrls.img = ["data-src", "src"];
        ctx.loaders.vue.transformAssetUrls.source = ["data-srcset", "srcset"];
      }

      //if (ctx.isClient) {
      config.module.rules.push({
        test: /\.vue$/,
        loader: "vue-svg-inline-loader",
        options: {
          /* ... */
        }
      });
      //}

      if (ctx.isClient) {
        config.resolve.alias = {
          ...config.resolve.alias,
          TweenLite: path.resolve(
            "node_modules",
            "gsap/src/uncompressed/TweenLite.js"
          ),
          TweenMax: path.resolve(
            "node_modules",
            "gsap/src/uncompressed/TweenMax.js"
          ),
          TimelineLite: path.resolve(
            "node_modules",
            "gsap/src/uncompressed/TimelineLite.js"
          ),
          TimelineMax: path.resolve(
            "node_modules",
            "gsap/src/uncompressed/TimelineMax.js"
          )
        };
      }

      if (ctx.isDev) {
        config.devtool = ctx.isClient ? "source-map" : "inline-source-map";
      }
    }
  }
};
